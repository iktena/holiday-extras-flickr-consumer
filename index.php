<html>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.6.9/angular-sanitize.js"></script>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>

<head>
	<title>HE Flickr API consumer</title>
	<link rel="stylesheet" type="text/css" href="present.css">

</head>
<body>

<div ng-app="he" ng-controller="ctrl">

	<p>
		Enter tags: 
		<input id="tags" type="text" name="tags" ng-model="tags" ng-change="load()" />
	</p>
	<div ng-repeat="item in items" style="border: 1px solid black; width: 30%; height: 60%; overflow: auto; margin: 20px; padding: 2px; display: inline-block;">
		<p class="imageTitle">
			<img class="img-responsive smaller" data-ng-src="{{ item.media.m }}" /><br />
			<a href="{{ item.link }}">{{ item.title }}</a>, by {{ getAuthor(item.author) }}
		</p>
		<p class="description">
			Description: <span ng-bind-html="trimAngular(item.description)" />
		</p>
		<p class="tags">
			Tags: <span ng-bind-html="item.tags" />
		</p>
	</div>

</div>


<script>
	var app = angular.module("he", ['ngSanitize']);

			app.controller("ctrl", function($scope, $http) {

				$scope.load = function() {
					$http.get("data.php?request=" + $scope.tags)
					.then(
						function (response) {
							$scope.json = response.data;
							console.log($scope.json);
							$scope.items = $scope.json.items;
						},
						function (response) {
							$scope.data = "Error occured " + response.status + " " + response.statusText + " " + response.data;
						}
					);
				};

				$scope.regex = '(<p>.*</p>){3}';

				$scope.trimAngular = function(data) {
					var occurs = ( data.match(/<p>/g) || []).length;
					if ( occurs == 2 )
						return "No description";
					else
					{
						var content = data.substring(data.lastIndexOf("<p>"));
						return content.substring(0, 100);
					}
				};

				$scope.getAuthor = function(data) {
					return data.substring(data.indexOf("\"") + 1, data.lastIndexOf("\""));
				};
		});
</script>

</body>
</html>